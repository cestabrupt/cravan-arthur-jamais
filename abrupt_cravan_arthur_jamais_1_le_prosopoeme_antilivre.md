---
author: "Arthur Cravan"
title: "Le prosopoème --- JAMAIS I"
publisher: "ABRÜPT"
date: "septembre 2018"
lang: "fr"
rights: "© 2018 ABRUPT, CC BY-NC-SA"
---

#

Enfin, dernière extravagance, j'imaginais le *prosopoème*, chose future, et dont je renvoyai, du reste, l'exécution aux jours heureux --- et combien lamentables --- de l'inspiration. Il s'agissait d'une pièce commencée en prose et qui insensiblement par des rappels --- la rime --- d'abord lointains et de plus en plus rapprochés, naissait à la poésie pure.

Puis je retombais dans mes tristes pensées.

# Poète et Boxeur

Houiaiaia ! Je partais dans 32 heures pour l'Amérique. De retour de
Bucarest, depuis 2 jours seulement j'étais à Londres et j'avais déjà
trouvé l'homme qu'il me fallait : qui me payait tous les frais de
déplacement pour une tournée de 6 mois, sans garantie, par exemple !
mais ça je m'en foutais. Et puis, je n'allais pas tromper ma femme !!!
merde, alors ! Et puis, vous ne devineriez jamais ce que je devais
faire : je devais lutter sous le pseudonyme de *Mysterious Sir Arthur
Cravan*, le poète aux cheveux les plus courts du monde, *petit-fils du
Chancelier de la Reine*, naturellement, *neveu d'Oscar Wilde*,
renaturellement, et *petit neveu de Lord Alfred Tennyson*,
rerenaturellement (je deviens intelligent). Ma lutte était quelque chose
de tout à fait nouveau : la lutte du Thibet, la plus scientifique
connue, bien plus terrible que le jiu-jitsu : une pression sur un nerf
ou un tendon quelconque et ftt ! l'adversaire \[qui n'était pas acheté
(rien qu'un tout petit peu)\] tombait comme foudroyé ! Il y avait de
quoi mourir de rire : houiaiaiaia ! sans compter que ça pouvait être de
l'or en barre, puisque j'avais calculé que si l'entreprise marchait bien
elle pouvait me rapporter dans les 50 000 francs, ce qui n'est pas à
dédaigner. En tous cas, ça valait toujours mieux que le truc de
spiritisme que j'avais commencé à monter.

J'avais 17 ans et j'étais villa et je rentrais porter, la nouvelle à ma
moitié qui était restée à l'hôtel, dans l'espoir d'en tirer quelque
chose, avec deux cons aux viandes ennuyées, un espèce de peintre et un
poète (rimons, rimu : ton nez dans mon cul) qui m'admiraient (tu
parles) ! et m'avaient rasé pendant près d'une heure avec des histoires
sur Rimbaud, le vers libre, Cézanne, Van Gogh, oh la la la la ! je crois
Renan et puis je ne sais plus quoi.

Je trouvais Madame Cravan seule et je lui dis ce qui m'était arrivé,
tout en faisant mes malles, car il s'agissait de faire vite. Je pliais,
en deux temps, trois mouvements mes chaussettes de soie à 12 frs. la
paire qui m'égalaient à Raoul le Boucher et mes chemises où traînaient
des restes d'aurore. Le matin, je donnais ma gaule diaprée à ma femme
légitime, je lui remis après cinq fraîches abstractions de 100 francs
chacune, puis j'allais faire mon pipi de cheval. Le soir, je jouais
quelques troumlalas sur mon violon ; je baisais la biseloquette de mon
bébé, et fis des câlin-câlin à mes beaux gosses. Puis, en attendant
l'heure du départ, et tout en rêvant à ma collection de timbres, je
foulais le plancher de mes pas d'éléphant et je balançais mon citron
splendide en respirant le parfum si touchant et partout répandu des
pets. 18 h. 15. Fuitt ! en bas les escaliers ! Je sautai dans un taxi.
C'était l'heure de l'apéritif : la lune immense comme un million
présentait beaucoup d'analogie avec une pilule digérée pour les lombagos
bleus. J'avais 34 ans et j'étais cigare. J'avais plié mes 2 mètres dans
l'auto où mes genoux avançaient deux mondes vitrés et j'apercevais sur
les pavés qui répandaient leurs arcs-en-ciel les cartilages grenats
croiser les bifstecks verts ; les spécimens d'or frôler les arbres aux
rayons irisés, les noyaux solaires des bipèdes arrêtés ; enfin, avec des
franges roses et des fesses aux paysages sentimentaux, les passants du
sexe adoré et, de temps à autre, je voyais encore, parmi les chieurs
enflammés, apparaître des phénix resplendissants.

Mon imprésario m'attendait, comme convenu, sur le quai 8 de la gare et
tout de suite je retrouvais avec plaisir sa vulgarité, sa joue que
j'avais déjà goûté comme du veau aux carottes, ses cheveux qui
fabriquaient du jaune et du vermillon, son intellect de coléoptère et,
près de la tempe droite, un bouton d'un charme unique ainsi que ses
pores rayonnants de son chronomètre en or.

Je choisis un coin dans le coupé de 1^re^ classe où je m'installais
confortablement. C'est-à-dire, j'appuyais mes assommoirs et j'allongeais
les jambes le plus simplement du monde.

\noindent Et sous mon crâne de homard je remuais mes globes de Champion du Monde

\noindent Afin de voir les gens réunis, et presque au hasard, quand

\noindent J'aperçus un monsieur, pharmacien ou notaire,

\noindent Qui sentait comme un concierge ou comme un pélican.

\noindent Hun, hun ! ça me plaisait : ses sentiments

\noindent Se développaient ainsi que chez un herbivore,

\noindent Tandis que sa tête me rappelait sérieusement

\noindent Les temps où je dormais dans l'intimité de ma grosse haltère, et, ma
foi, dans une espèce d'adoration très réelle et autre chose de difficile
à exprimer

\hfill devant l'égoïste nacré,

\noindent Que j'embouteillais de mes yeux atlantiques,

\noindent J'admirais l'avant-bras comme un morceau sacré

\noindent Et comparais le ventre à l'attrait des boutiques.

\noindent Les billets, s'il vous plaît !

\noindent Nom d'un chien ! je suis sûr et certain que 999 personnes sur 1 000
eussent été complètement bouleversées dans leur gustation par la voix du
contrôleur. J'en suis persuadé et pourtant, j'affirme en toute sincérité
qu'elle ne me causa aucune gêne, mais, qu'au contraire, dans le
compartiment homogène le timbre avait la douceur qu'ont les zouizouis
des petits oiseaux. La beauté des banquettes en fut, si possible,
augmentée, à tel point que je me demandais si je n'étais pas victime
d'un commencement d'ataxie et ce, d'autant plus que je fixais toujours
le sacré petit bourgeois, si tendre en son trou du cul, en me demandant
ce que pouvait avoir de bien particulier l'allure du poids lourd qui en
face de moi semblait roupiller profondément. Je pensais : oh ! jamais
moustache n'a dégagé une si intense corporalité, et surtout, nom de
Dieu ! que je t'aime :
{\centering Et, tandis qu'allophage\footnote{Néologisme servant à désigner celui qui déguste ou mange idéalement autrui.}

A l'amour de ton chauffage,

Nos gilets

Se câblent leurs violets,

Que, chéri et choufleur,

Je suis tes gammes

Et tes couleurs,

Et, qu'en un amalgame

De Johnson, de phoque et d'armoire

Nos merdes rallument leurs moires,

Fff ! les pistons

Du veston.

Dans la finale

Abdominale !
}

\noindent Tous les propriétaires sont des termites, dis-je subitement, histoire de
réveiller le petit vieux, dont j'étais plein, et de le scandaliser.
Puis, en le regardant dans le blanc des yeux, une seconde fois : Oui,
parfaitement, Monsieur, je ne crains pas de le répéter, dussé-je me
compromettre, qu'à mon corps défendant et la biqueamichère de mon
ratazouin que tous les propriétaires sont des *term\-\-\-mites*. À son air
excessivement emmerdé, je voyais bien qu'il me prenait pour un fou ou un
terrible voyou, mais qu'il faisait semblant de ne pas comprendre
tellement il avait peur que je lui aplatisse mon poing sur la gueule.

Fallait-il quand même que je sois bête, surtout avec ma mentalité, pour
n'avoir pas remarqué plus tôt une Américaine avec sa fille qui me
faisait presque vis-à-vis. Il avait été nécessaire pour attirer mon
attention que la mère allât aux cabinets où, du reste, je restais avec
elle sentimentivement

\noindent\hspace{4em} Songeant comme à sa bourse à ses déjections,

\noindent Et lorsqu'elle eu repris sa place j'enviais ses boucles d'oreilles et
j'imaginais qu'elle est

\hfill belle avec son argent

\noindent et, malgré ses rides et sa vieille carcasse :

\noindent Vraiment qu'elle a du charme

\hfill pour un cœur guidé par l'intérêt

\noindent qui se fiche pas mal de tout pourvu que ça lui rapporte et je me disais rageusement :

\noindent Rrr ! pour te masturber, t'entraînant aux chiottes tiens ! je t'ferai
minette, vieille salope !

Ce qu'il y a de plus amusant et ce qui est bien de moi c'est qu'en
m'occupant ensuite de la plus jeune, après avoir rêvé d'extorquer par
tous les moyens des fonds à sa mémère, j'en étais venu, avec ma satanée
nature à souhaiter une existence bourgeoise en sa compagnie. C'est vrai
et je ne pouvais m'empêcher de penser : mon vieux, quel drôle de coco tu
fais. Tu sais, petite, tu pourrais orienter ma vie d'une façon toute
différente. Ah ! si seulement tu voulais m'épouser. Je serai gentil avec
toi et nous irons partout

\hfill achetant le bonheur

\noindent mais nous habiterons un chic hôtel à San-Francisco. Mon imprésario, je
m'en moque (il ne s'en doute pas, la vache !)

\noindent Nous passerons des
après-midi entières à nous aimer assis sur les canapés du salon, les
têtes en plongée et les ventres lucides. À tes moindres exigences nous
sonnerons les bonnes. Vois-tu les tapis jetteront leurs flammes

\begin{Center} Les tableaux de valeur, les meubles engraissants :

Les bahuts en boule et les dressoirs centrés,

Aux plexus rougissants,

Boucheront jusqu'aux bords nos organes dorés.

Les murs paralytiques,

Éliminant les saphirs,

Exécuteront des gymnastiques

D'ibis et de tapir ;

Sur les fauteuils charmés,

Avec nos pieds palmés,

Nous reposerons nos pectoraux trop lourds,

Et savourerons

Dans les ronrons

Nos langues supérieures aux marennes

Et feront dans le satin les vesses de velours.

Pareilles aux pâtes, les pensées banales

Nous bourrerons comme des oies,

Pendant que nos estomacs liés,

Plus fort que deux souliers,

Tout en répandant la chaleur du foie,

Se baigneront dans leurs aurores intestinales.
\end{Center}

I say, boy ! here we are : Liverpool, c'était la voix de mon manager.

\noindent Allllright.

\RaggedLeft A. C.
