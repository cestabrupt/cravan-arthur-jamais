---
author: "Arthur Cravan"
title: "Je suis toutes les choses --- JAMAIS III"
publisher: "ABRÜPT"
date: "septembre 2018"
lang: "fr"
rights: "© 2018 ABRUPT, CC BY-NC-SA"
---

\begin{Parallel}[p]{\textwidth}{\textwidth}
\ParallelLText{%
\begin{FlushRight}
\vspace*{3cm}
{\scshape Sifflet}
\vspace{1.5cm}

Le rythme de l'océan berce les transatlantiques,

Et dans l'air où les gaz dansent tels des toupies,

Tandis que siffle le rapide héroïque qui arrive au Havre,

S'avançent comme des ours, les matelots athlétiques.

New York ! New York ! Je voudrais t'habiter !

J'y vois la science qui se marie

À l'industrie,

Dans une audacieuse modernité.

Et dans les palais,

Des globes,

Éblouissants à la rétine,

Par leurs rayons ultra-violets ;

Le téléphone américain,

Et la douceur

Des ascenseurs...

Le navire provoquant de la Compagnie Anglaise

Me vit prendre place à bord terriblement excité,

Et tout heureux du confort du beau navire à turbines,

Comme de l'installation de l'électricité,

Illuminant par torrents la trépidante cabine.

La cabine incendiée de colonnes de cuivre,

Sur lesquelles, des secondes, jouirent mes mains ivres

De grelotter brusquement dans la fraîcheur du métal,

Et doucher mon appétit par ce plongeon vital,

Tandis que la verte impression de l'odeur du vernis neuf

Me criait la date claire, où, délaissant les factures,

Dans le vert fou de l'herbe, je roulais comme un œuf.

Que ma chemise m'enivrait ! et pour te sentir frémir

À la façon d'un cheval, sentiment de la nature !

Que j'eusse voulu brouter ! que j'eusse voulu courir !

Et que j'étais bien sur le pont, ballotté par la musique ;

Et que le froid est puissant comme sensation physique,

Quand on vient à respirer !

Enfin, ne pouvant hennir, et ne pouvant nager,

Je fis des connaissances parmi les passagers,

Qui regardaient basculer la ligne de flottaison ;

Et jusqu'à ce que nous vîmes ensemble les tramways du matin courir à l'horizon,

Et blanchir rapidement les façades des demeures,

Sous la pluie, et sous le soleil, et sous le cirque étoilé,

Nous voguâmes sans accident jusqu'à sept fois vingt-quatre heures !

Le commerce a favorisé ma jeune initiative :

Huit millions de dollars gagnés dans les conserves

Et la marque célèbre de la tête de Gladstone

M'ont donné dix steamers de chacun quatre milles tonnes,

Qui battent des pavillons brodés à mes initiales,

Et impriment sur les flots ma puissance commerciale.

Je possède également ma première locomotive :

Elle souffle sa vapeur, tels les chevaux qui s'ébrouent,

Et, courbant son orgueil sous les doigts professionnels,

Elle file follement, rigide sur ses huit roues.

Elle traîne un long train dans son aventureuse marche,

Dans le vert Canada, aux forêts inexploitées,

Et traverse mes ponts aux caravanes d'arches,

À l'aurore, les champs et les blés familiers ;

Ou, croyant distinguer une ville dans les nuits étoilées,

Elle siffle infiniment à travers les vallées,

En rêvant à l'oasis : la gare au ciel de verre,

Dans le buisson des rails qu'elle croise par milliers,

Où, remorquant son nuage, elle roule son tonnerre.
\end{FlushRight}
}%

\ParallelRText{%
  \begin{FlushLeft}
\vspace*{3cm}
{\scshape Hie !}
\vspace{1.5cm}

Quelle âme se disputera mon corps ?

J'entends la musique :

Serai-je entraîné ?

J'aime tellement la danse

Et les folies physiques

Que je sens avec évidence

Que, si j'avais été jeune fille,

J'eusse mal tourné.

Mais, depuis que me voilà plongé

Dans la lecture de cet illustré,

Je jurerai n'avoir vu de ma vie

D'aussi féériques photographies :

L'océan paresseux berçant les cheminées,

Je vois dans le port, sur le pont des vapeurs,

Parmi des marchandises indéterminées,

Les matelots se mêler aux chauffeurs ;

Des corps polis comme des machines,

Mille objets de la Chine,

Les modes et les inventions ;

Puis, prêts à traverser la ville,

Dans la douceur des automobiles,

Les poètes et les boxeurs.

Ce soir, quelle est ma méprise,

Qu'avec tant de tristesse,

Tout me semble beau ?

L'argent qui est réel,

La paix, les vastes entreprises,

Les autobus et les tombeaux ;

Les champs, le sport, les maîtresses,

Jusqu'à la vie inimitable des hôtels.

Je voudrais être à Vienne et à Calcutta,

Prendre tous les trains et tous les navires,

Forniquer toutes les femmes et bâfrer tous les plats.

Mondain, chimiste, putain, ivrogne, musicien, ouvrier, peintre, acrobate, acteur ;

Vieillard, enfant, escroc, voyou, ange et noceur ; millionnaire, bourgeois, cactus, giraffe ou corbeau ;

Lâche, héros, nègre, singe, Don Juan, souteneur, lord, paysan, chasseur, industriel,

Faune et flore :

Je suis toutes les choses, tous les hommes et tous les animaux !

Que faire ?

Essayons du grand air,

Peut-être y pourrai-je quitter

Ma funeste pluralité !

Et tandis que la lune,

Par delà les marronniers,

Attelle ses lévriers,

Et, qu'ainsi qu'en un kaléidoscope,

Mes abstractions

Élaborent les variations

Des accords

De mon corps,

Que mes doigts collés

Au délice de mes clés

Absorbent de fraîches syncopes,

Sous des motions immortelles

Vibrent mes bretelles ;

Et, piéton idéal

Du Palais-Royal,

Je m'enivre avec candeur

Même des mauvaises odeurs.

Plein d'un mélange

D'éléphant et d'ange,

Mon lecteur, je ballade sous la lune

Ta future infortune,

Armée de tant d'algèbre,

Que, sans désirs sensuels,

J'entrevois, fumoir du baiser,

Con, pipe, eau, Afrique et repos funèbre,

Derrière les stores apaisés,

Le calme des bordels.

Du baume, ô ma raison !

Tout Paris est atroce et je hais ma maison.

Déjà les cafés sont noirs.

Il ne reste, ô mes hystéries !

Que les claires écuries

Des urinoirs.

Je ne puis plus rester dehors.

Voici ton lit ; sois bête et dors.

Mais, dernier des locataires,

Qui se gratte tristement les pieds,

Et, bien que tombant à moitié,

Si j'entendais sur la terre

Retentir les locomotives,

Que mes âmes pourtant redeviendraient attentives !
  \end{FlushLeft}
}%
\end{Parallel}
