# ~/ABRÜPT/ARTHUR CRAVAN/JAMAIS/*

La [page de cette série](https://abrupt.ch/arthur-cravan/jamais/) sur le réseau.

## Sur la série de livres

As-tu déjà essayé de mettre les doigts dans la prise pour recouvrer la saveur de l’électricité ? Le court-circuit des neurones, jamais ? Jamais… Tu as bien raison. Ne fais pas ça ! Lis ce livre à la place ! Maintenant !

Cette série *JAMAIS* contient :

- Le prosopoème, JAMAIS I
- Réclame, JAMAIS II
- Je suis toutes les choses, JAMAIS III

## Sur l'auteur

Fabian Avenarius Lloyd, dit Arthur Cravan. Date de naissance : 22 mai 1887. Lieu de naissance : Lausanne (sujet britannique ; déteste les paysages alpins). Date de décès : jamais. Lieu de décès : à déterminer. Professions : chevalier d’industrie, marin sur le Pacifique, muletier, cueilleur d’oranges en Californie, charmeur de serpents, rat d’hôtel, neveu d’Oscar Wilde, bûcheron dans les forêts géantes, ex-champion de France de boxe, petit-fils du chancelier de la reine, chauffeur d’automobile à Berlin, cambrioleur, etc. Rien de plus. Si ce n’est qu’Arthur Cravan mérite d’être dieu, ou tout du moins empereur dès son retour du Mexique. Et que la chose soit entendue auprès des camarades de la démocratie, etc., etc., si Arthur veut bien s’en revenir de son escapade, ce sera la Cravanocratie. Et nous cravanerons, tous ! La littérature bourre-pif s’apprêtera de flanelles, et avec elle, nous serons chics.

## Sur la licence

Ces [antilivres](https://abrupt.ch/antilivre/) sont disponibles sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
