---
author: "Arthur Cravan"
title: "Réclame --- JAMAIS II"
publisher: "ABRÜPT"
date: "septembre 2018"
lang: "fr"
rights: "© 2018 ABRUPT, CC BY-NC-SA"
---

\p J'étais descendu dans mon ventre, et je devais commencer à être dans un
état féerique ; car mon tube digestif était suggestif ; ma cellule folle
dansait ; et mes souliers me paraissaient miraculeux.

\p Différentes choses. Nous sommes heureux d'apprendre la mort du peintre Jules Lefebvre. Le 3 avril, au Cirque de Paris, le nègre Gunther et Georges Carpentier se rencontreront dans un beau match de boxe. Le bruit que fait Marinetti est pour nous plaire : car la gloire est un scandale.

\p Que faut-il au Poète ? Une bonne nourriture. Où peut-il la trouver à bon marché ? Chez Jourdan. Dix, rue des Bons-Enfants, dix (près des Grands Magasins du Louvre).

\p Crémerie de Cluny. Cinquante-sept, rue Saint-Jacques, cinquante-sept. Beurre et œufs de première qualité. Légumes cuits surfins préparés par la maison. Maison recommandée à Messieurs les Étudiants.

\p Librairie. Papeterie. Articles de bureaux et d'écoliers. Fantaisies. Monsieur Lucain. Soixante-cinq, boulevard Saint-Germain. Paris.

\p Où se rencontrent les poètes ?... les marlous ?... les boxeurs ?... Chez Jourdan. Restaurateur. Dix, rue des Bons-Enfants, dix (près des Grands Magasins du Louvre).

\p Vous ne pouvez pas tout casser mais... avec de la Seccotine vous pouvez tout coller réparer.

\p G. Séveau. Artiste Peintre. Nonante et un, rue de l’Amiral-Mouchez. Paris treizième. Couleurs fines. Broyées à la main.

\p Prenez garde à la peinture ! La bonne se trouve Galerie Clovis Sagot. Quarante-six, rue Laffitte, quarante-six. Dépôt de la Revue “Maintenant”.

\p Où peut-on voir van Dongen mettre la nourriture dans sa bouche, la mâcher, digérer et fumer ? Chez Jourdan. Restaurateur. Dix, rue des Bons-Enfants, dix (près des Grands Magasins du Louvre).

\p Nouveau Cirque. Deux cent quarante-sept, rue Saint-Honoré. Directeur : Charles Debray. À partir du quinze octobre à neuf heures du soir. Grand Championnat de Lutte de Combat. Peterson --- Wuong-Fan-Tzin. Raoul de Rouen --- Saft. Maurice Deriaz --- Zbysko. Cherpillod --- Myake. Etc. etc. etc.

\p Automobiles. Mercédès. Nouvelle installation. Cent cinquante-quatre, Champs-Elysées. En face de l'Hôtel Astoria.

\p Automobiles. Mercédès. Voitures de dix à nonante horse power. Tous types de carosserie. Prêtes à prendre la route.

\p Une Bonbonnière à proximité de l'Opéra. Restaurant Français. Six, rue du Helder, six. Cuisine soignée. Caves renommées. Directeur : P. Driessens.

\p À l'automne. Ouverture du Rio Tinto. Plaisirs nouveaux.

\p Les noctambules. Sept, rue Champollion, sept (Quartier Latin). Le Vendredi, six mars, à neuf heures du Soir. Arthur Cravan. Conférenciera. Dansera. Boxera. Entrée : deux francs cinquante.

\p Du deux mille cinq cents pour cent ! Spéculateurs ! Achetez de la peinture. Ce que vous paierez deux cents francs aujourd'hui vaudra dix mille francs dans dix ans. Vous trouverez les jeunes Galerie Malpel. Quinze, rue Montaigne, quinze. Dépôt de la Revue “Maintenant”.

\p La nourriture est le foyer des sentiments. Les Hommes peuvent charger leurs corps chez Jourdan. Restaurateur. Dix, rue des Bons-Enfants, dix (La Maison Blanche). À proximité des Grands Magasins du Louvre.

\p Critique. N'ayant rien reçu ni des auteurs ni des femmes de lettres (bêêêh, bêêêh !) je suis bien obligé de remettre à la prochaine fois ma petite chronique où l'on verra ma manière de traiter les gens qui font de l'art. Robert Miradique.

\p Pff. Il est plus méritoire de découvrir le mystère dans la lumière que dans l'ombre. Tout grand artiste a le sens de la provocation. Les abrutis ne voient le beau que dans les belles choses. Marie Lowitska.
